import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResistorcolorcodeComponent } from './resistorcolorcode/resistorcolorcode.component';

const routes: Routes = [
  { 
    path: '',
    component: ResistorcolorcodeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
