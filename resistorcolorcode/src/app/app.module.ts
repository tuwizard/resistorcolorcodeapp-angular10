import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ResistorcolorcodeComponent } from './resistorcolorcode/resistorcolorcode.component';
import { ColorCodeService } from './services/colorcode.service';

@NgModule({
  declarations: [
    AppComponent,
    ResistorcolorcodeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ColorCodeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
