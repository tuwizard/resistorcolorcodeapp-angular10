export class ColorCodeViewModel {
    public Id: number;
    public Color: string;
    public Digit: number;
    public Multiplier: number;
    public Tolerance: number;    
}

export class ResistorColorCodesViewModel {
    public BaseValue: number;
    public Tolerance: string;
    public MinValue: number;
    public MaxValue: number;
}