import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResistorcolorcodeComponent } from './resistorcolorcode.component';

describe('ResistorcolorcodeComponent', () => {
  let component: ResistorcolorcodeComponent;
  let fixture: ComponentFixture<ResistorcolorcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResistorcolorcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResistorcolorcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
