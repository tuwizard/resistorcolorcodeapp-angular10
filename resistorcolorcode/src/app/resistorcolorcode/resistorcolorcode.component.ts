import { Component, OnInit, OnDestroy } from '@angular/core';
import { ColorCodeViewModel, ResistorColorCodesViewModel } from '../models/colorcode';
import { ColorCodeService } from '../services/colorcode.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-resistorcolorcode',
  templateUrl: './resistorcolorcode.component.html',
  styleUrls: ['./resistorcolorcode.component.css']
})
export class ResistorcolorcodeComponent implements OnInit, OnDestroy {
  pageTitle: string = 'Resistor Color Code';  
  selectedColorCodeVM: ColorCodeViewModel;
  colorCodeOptions: ColorCodeViewModel[];
  submitted: boolean = false;
  errorMsg: string = '';
  public year: number = new Date().getFullYear(); // Set the current year
  resistorColorCodeVM: ResistorColorCodesViewModel;
  resistorOutput: ResistorColorCodesViewModel;
  resistorOutput2: ResistorColorCodesViewModel;
  
  bandAColorCode: ColorCodeViewModel;
  bandBColorCode: ColorCodeViewModel;
  bandCColorCode: ColorCodeViewModel;
  bandDColorCode: ColorCodeViewModel;
  colorCodeSub: Subscription;

  constructor(private colorCodeService: ColorCodeService) { }

  ngOnInit(): void {
    this.colorCodeSub = this.colorCodeService.getColorCodes().subscribe( res => {
      if (res) {
        this.colorCodeOptions = <ColorCodeViewModel[]>res;
        this.selectedColorCodeVM = this.colorCodeOptions[0];
        this.bandAColorCode = this.selectedColorCodeVM;
        this.bandBColorCode = this.selectedColorCodeVM;
        this.bandCColorCode = this.selectedColorCodeVM;
        this.bandDColorCode = this.selectedColorCodeVM;
      } else {
        console.log('error no data');
      }
    })
  }

  updateSelectedOption(eventObj: any, bandx: string) {
    if (eventObj) {
      if (bandx === 'bandA') {
        this.bandAColorCode = eventObj;
      } 
      if (bandx === 'bandB') {
        this.bandBColorCode = eventObj;
      } 
      if (bandx === 'bandC') {
        this.bandCColorCode = eventObj;
      } 
      if (bandx === 'bandD') {
        this.bandDColorCode = eventObj;
      } 
    }
  }

  onSubmit() {
    this.submitted = true;
    this.resistorOutput = { 
      BaseValue: this.calculateOhmValue(false, true),
      Tolerance: this.bandDColorCode.Color,
      MinValue: this.calculateOhmValue(true, false),
      MaxValue: this.calculateOhmValue(false, false)
    }
  }

  /// <summary>
  /// Calculate the Ohm value of a resistor based on the band color.
  /// </summary>
  /// <param name="bandAColor">The color of the first figure of component value band.</param>
  /// <param name="bandBColor">The color of the second significant figure band.</param>
  /// <param name="bandCColor">The color of the decimal multiplier band.</param>
  /// <param name="bandDColor">The color of the tolerance value band.</param>
  /// <param name="isMinValue">check min value or not</param>
  /// <param name="baseValue">base value</param>
  /// <returns></returns>
  calculateOhmValue(isMinValue: boolean = false, baseValue:boolean = true): number {
    let retOhmValue: number = 0;

    let ohmValue: string = this.bandAColorCode.Digit.toString() + this.bandBColorCode.Digit.toString();
    if (ohmValue !== '') {
      retOhmValue = this.toNumber(ohmValue);
      if (this.bandCColorCode.Multiplier.toString() !== '')
      {
        retOhmValue *= this.bandCColorCode.Multiplier;
      }

      if (this.bandDColorCode.Tolerance > 0 && !baseValue) {
        if (isMinValue) {
          retOhmValue = retOhmValue - ((retOhmValue * this.bandDColorCode.Tolerance) / 100);
        } else {
          retOhmValue = retOhmValue + ((retOhmValue * this.bandDColorCode.Tolerance) / 100);
        }
      }
    }

    return retOhmValue;
  }

  toNumber(input: string) {     
    if (!input) return NaN;

    if (input.trim().length==0) { 
        return NaN;
    }
    return Number(input);
  }
  ngOnDestroy() {
    // Clean up code: Unsubcribe to all observable, detach all event handler.
    this.colorCodeSub.unsubscribe();
  }

}

