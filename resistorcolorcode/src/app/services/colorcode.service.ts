import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http';
import { ColorCodeViewModel } from '../models/colorcode';

@Injectable()
export class ColorCodeService {
    colorCodes: ColorCodeViewModel[];

    constructor(private http: HttpClient) {
    }
    getColorCodes<ColorCodeViewModel>(): Observable<ColorCodeViewModel[]> {
      return this.http.get<ColorCodeViewModel[]>('assets/data/ResistorColorCode.json'); // or from API: environment.resistorcolorcode.getRCCList (Take a look resistorcolorcode-demo the .NET CORE WebAPI project)
    }

    getSelectedResistorColorCode(): Observable<string> {
      return this.http.get<string>('assets/data/SelectedResistorColorCode.json'); // or from API: environment.resistorcolorcode.getSelectedRCC
    }
}
